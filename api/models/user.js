'use strict'

const Sequelize = require("sequelize")
var connection = require('./../database')

const moment = require('moment')


const User = connection.define("user",{
	name: Sequelize.STRING,
	email: Sequelize.STRING,
	facebookId: Sequelize.STRING,
	facebookToken: Sequelize.STRING,
	lastLogin: 'DATETIME'
})

connection.sync()

module.exports = User

