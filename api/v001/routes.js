'use strict'


const authApi = require('./controllers/auth-api')
const user = require('./controllers/users')





module.exports.initialize = function(app, version) {

  app.get({ path: '/hello', version: version }, function (req, res, next) {
    res.send(200, 'Hello from version ' + version);
    return next();
  })


  app.put({ path: '/auth/login', version: version }, authApi.logIn)
  app.put({ path: '/auth/logout', version: version }, authApi.logOut)
  
/*
  app.get({ path: '/users', version: version }, authApi.validate, user.model.query())
  app.get({ path: '/users/:id', version: version }, authApi.validate, user.model.detail())
  app.post({ path: '/users', version: version }, authApi.validate, user.model.insert())
  app.put({ path: '/users/:id', version: version }, authApi.validate, user.model.update())
  app.del({ path: '/users/:id', version: version }, authApi.validate, user.model.remove())
  app.put({ path: '/user/update', version: version }, authApi.validate, user.update)
*/

}