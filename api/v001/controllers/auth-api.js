'use strict'


const User = require('../../models/user')
const restifyMongoose = require('restify-mongoose')
const moment = require('moment')



/*  --------------------    CUSTOM REQUESTS    --------------------- */

module.exports.logIn = function(req, res, next) {

  //console.log('body: ', req.body)
  //console.log('params:', req.params)
  //req.params
  var lastLoginDate = moment(moment()).format("YYYY-MM-DD HH:mm:ss")
  var data = req.params
  data.lastLogin = lastLoginDate

  console.log(data,data.facebookId)
  User.findAll({
    where: {
      facebookId: data.facebookId
    }
  }).then(function(userDoc) {
    if (userDoc.length > 0) {

      // update
      User.update({ lastLogin: lastLoginDate }, {
        where: {
          facebookId: data.facebookId
        }
      }).then(function(newUserDoc) {
        console.log('UPDATED !!!!')
        res.send(newUserDoc)
      }).catch(function(error) {
        console.error("update fail: ", error);
        res.json(500, error);
      });

      return next()
    }


    // create 
    User.create(data)
      .then(function(data) {
        res.send(data)
      })
      .catch(function(error) {
        console.error("Create fail: ", error);
        res.json(500, error);
      });

  })

  return next()
}

module.exports.logOut = function(req, res, next) {
  // XXX: delete token
  res.send(200, { message: 'Session closed' })
}




/*  --------------------    PRIVATE FUNCTIONS     --------------------- */


function saveToken(origin, data, token) {

}