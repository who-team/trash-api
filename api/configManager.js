'use strict'


const configTester = require('./config-tester')
const configProduction = require('./config-production')

var config 

module.exports.setTesterMode = function(val) {
  if (val) {
    config = configTester
  } else {
    config = configProduction
  }

}

module.exports.getConfig = function(){
  return config
}

