'use strict'


//const mysql = require("mysql")
const Sequelize = require("sequelize")
const config = require('./configManager').getConfig()

const connection = new Sequelize(config.DATABASE_NAME, config.DATABASE_USER, config.DATABASE_PASSWORD, {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },


  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
})

/*
const connection = mysql.createConnection({
  host: config.DATABASE_HOST,
  database: config.DATABASE_NAME,
  user: config.DATABASE_USER,
  password: config.DATABASE_PASSWORD
})
*/
/*
function connect() {
  connection.open('localhost', config.DATABASE_NAME).then(
    () => { },
    err => { }
  )
}

connect();
*/
module.exports = connection;